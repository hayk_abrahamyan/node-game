const {check, validationResult} = require("express-validator");

const validatorFunction = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.json({
            errors: errors.array(),
        });
    }
    next();
}

exports.get = [
    check("user_name", "User name is required").exists(),
    validatorFunction
]

