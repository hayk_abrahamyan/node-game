const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  right: {
    type: Number,
    default: 0
  },
  wrong: {
    type: Number,
    default: 0
  },

});

module.exports = model("User", userSchema);
